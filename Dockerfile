FROM golang:1.12.5 as builder
WORKDIR /app
COPY ./app/ .
RUN go build

FROM alpine
WORKDIR /app
COPY --from=builder ./app/hello .
CMD ["./hello"]

